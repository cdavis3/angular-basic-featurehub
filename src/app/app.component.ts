import { Component, OnInit } from '@angular/core';
import {
  EdgeFeatureHubConfig,
  ClientContext
} from 'featurehub-eventsource-sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  title = 'angular-basic-featurehub';

  fhContext: ClientContext;

  ngOnInit() {
    const fhConfig = new EdgeFeatureHubConfig('http://localhost:4321', 'default/3969b935-a139-4d6e-8c23-37256aa2db4f/OuSxmsZngSY9YqAj1tWR1PplaO9OuRM8Pd5Ioz0n')
    fhConfig.newContext().build().then(context => this.fhContext = context);
  }
}
